package cmpe434lab9.Controllers;

import cmpe434lab9.Globals.Actuators;
import cmpe434lab9.Tasks.GotoLightSource;

public class Main {
	public static void main(String[] args) {
		Actuators.init();
		new GotoLightSource().run();
	}
}
