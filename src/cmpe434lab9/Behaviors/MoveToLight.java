package cmpe434lab9.Behaviors;

import cmpe434lab9.Globals.Actuators;
import cmpe434lab9.Globals.LCD;
import cmpe434lab9.Globals.Sensors;
import lejos.robotics.subsumption.Behavior;

public class MoveToLight implements Behavior{

	private boolean suppressed;
	
	@Override
	public boolean takeControl() {
		return SearchLight.lightSourceFound;
	}

	@Override
	public void action() {
		suppressed=false;
		LCD.clear();
		LCD.print("MoveToLight", 0, 0);
		Actuators.pilot.forward();
		while(!suppressed){
	
			Thread.yield();
		}
		Actuators.pilot.stop();
	}

	@Override
	public void suppress() {
		suppressed=true;
	}

}
