package cmpe434lab9.Behaviors;

import cmpe434lab9.Globals.Actuators;
import cmpe434lab9.Globals.LCD;
import cmpe434lab9.Globals.Sensors;
import lejos.hardware.motor.Motor;
import lejos.robotics.subsumption.Behavior;

public class SearchLight  implements Behavior {

	private boolean suppressed;
	public static boolean lightSourceFound=false;
	
	@Override
	public boolean takeControl() {
		return true;
	}

	@Override
	public void action() {
		suppressed=false;
		Sensors.resetGyro();
		Actuators.pilot.rotate(360,true);
		float[] degree_light=new float[2];
		float degree=degree_light[0]=0;
		degree_light[1]=Sensors.getLight();
		while(!suppressed && degree<360){
			Thread.yield();
			float light=Sensors.getLight();
			degree=Sensors.readGyro();
			if(degree_light[1]<light){
				degree_light[1]=light;
				degree_light[0]=degree;
			}
			LCD.clear();
			LCD.print("degree: "+degree, 0,-20);
			LCD.print("light: "+light, 0,0);
			LCD.print("pair: "+degree_light[0]+ " "+degree_light[1], 0, +20);
		}
		Actuators.pilot.stop();
		Actuators.pilot.rotate(degree_light[0],true);
		degree=0;
		Sensors.resetGyro();
		while(!suppressed && Sensors.readGyro() < degree_light[0]){
			Thread.yield();
		}
		Actuators.pilot.stop();
		if(!suppressed)
			lightSourceFound=true;
	}

	@Override
	public void suppress() {
		suppressed=true;
	}

}