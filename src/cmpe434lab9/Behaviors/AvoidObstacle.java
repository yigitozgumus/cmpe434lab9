package cmpe434lab9.Behaviors;

import java.awt.Desktop.Action;

import cmpe434lab9.Globals.Actuators;
import cmpe434lab9.Globals.LCD;
import cmpe434lab9.Globals.Sensors;
import lejos.robotics.subsumption.Behavior;

public class AvoidObstacle implements Behavior {

	//private static boolean suppressed;
	private static final float TRESHOLD=9f;
	
	@Override
	public boolean takeControl() {
		return Sensors.touchSensorPressed();
	}

	@Override
	public void action() {
		LCD.clear();
		LCD.print("AvoidObstacle", 0, 0);
		SearchLight.lightSourceFound=false;
		Actuators.pilot.travel(-10,true);
		while( Actuators.pilot.isMoving())  
			Thread.yield();
		Actuators.pilot.rotate(90,true);
		Sensors.resetGyro();
		for(int i=0; i<1000;i++);
		while(Sensors.readGyro() < 90 && Actuators.pilot.isMoving())
			Thread.yield();
	
		Actuators.pilot.stop();
		Sensors.resetGyro();
		Actuators.mediumRegulatedMotor.rotate(-90);
		float max=0;
		int loop=0;
		float travelled=0;
		for(int i=0; i<4; i++){
			Actuators.pilot.travel(50,true);
			for(int j=0; j<1000;j++);
			while( Sensors.readUltrasonic()<TRESHOLD 
					&& Actuators.pilot.isMoving()){
				float light=Sensors.getLight();
				if(max<light){
					max=light;
					loop=i;
					travelled=Actuators.pilot.getMovement().getDistanceTraveled();
				}
				Thread.yield();
			}
			Actuators.pilot.travel(15,false);
			Sensors.resetGyro();
			Actuators.pilot.rotate(-90,true);
			Actuators.mediumRegulatedMotor.rotate(+90,true);
			for(int j=0; j<1000;j++);
			while(Sensors.readGyro()>-90 && Actuators.pilot.isMoving())
				Thread.yield();
			Actuators.pilot.travel(15,false);
		}
		for(int i=0; i<=loop; i++){
			Actuators.pilot.travel(i==loop?travelled:50,true);
			for(int j=0; j<1000;j++);
			while(Sensors.readUltrasonic()<TRESHOLD &&
					Actuators.pilot.isMoving())
				Thread.yield();
			if(i==loop){
				Actuators.mediumRegulatedMotor.rotate((loop-1)*-90);
				break;
			}
			Actuators.pilot.travel(15,false);
			Sensors.resetGyro();
			Actuators.pilot.rotate(-90,true);
			Actuators.mediumRegulatedMotor.rotate(+90,true);
			for(int j=0; j<1000;j++);
			while(Sensors.readGyro()>-90 && Actuators.pilot.isMoving())
				Thread.yield();
			Actuators.pilot.travel(15,false);
		}
		Sensors.resetGyro();
		
	}

	@Override
	public void suppress() {
		//You shall not suppress
	}

}
