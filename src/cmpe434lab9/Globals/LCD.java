package cmpe434lab9.Globals;

import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.GraphicsLCD;

public class LCD {
	private static EV3 ev3 = (EV3) BrickFinder.getDefault();
	private static GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
	
	public static void print(String text,int x,int y){
		graphicsLCD.drawString(text, graphicsLCD.getWidth()/2+x, graphicsLCD.getHeight()/2+y,
				GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
	}
	public static void clear(){
		graphicsLCD.clear();
	}
}
