package cmpe434lab9.Globals;

import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.ColorAdapter;
import lejos.robotics.LightDetectorAdaptor;
import lejos.robotics.SampleProvider;
import lejos.robotics.TouchAdapter;

public class Sensors {
	//Sensors
	private static EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S4);
	private static EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S1);
	private static EV3UltrasonicSensor ultrasonicSensor = new EV3UltrasonicSensor(SensorPort.S3);
	private static EV3TouchSensor touchSensor = new EV3TouchSensor(SensorPort.S2);
	
	//Helpers
	private static ColorAdapter colorAdapter = new ColorAdapter(colorSensor);
	private static LightDetectorAdaptor lightDetectorAdaptor = new LightDetectorAdaptor((SampleProvider)colorSensor);
	private static SampleProvider sampleProviderUltrasonic = ultrasonicSensor.getDistanceMode();
	private static SampleProvider sampleProviderGyro = gyroSensor.getAngleAndRateMode();
	private static TouchAdapter touchAdapter = new TouchAdapter(touchSensor);
	
	public static float readUltrasonic(){
	    float [] sample = new float[sampleProviderUltrasonic.sampleSize()];
    	sampleProviderUltrasonic.fetchSample(sample, 0);
		return sample[0];
	}
	public  static int getRed(){
		return colorAdapter.getColor().getRed();
	}
	public  static int getBlue(){
		return colorAdapter.getColor().getBlue();
	}
	public  static int getGreen(){
		return colorAdapter.getColor().getGreen();
	}
	public static float getLight(){
		return lightDetectorAdaptor.getLightValue();
	}
	public static float readGyro(){
		float [] sample = new float[sampleProviderGyro.sampleSize()];
    	sampleProviderGyro.fetchSample(sample, 0);
    	return sample[0];
	}
	public static void resetGyro(){
		gyroSensor.reset();
	} 
	public static boolean touchSensorPressed(){
		return touchAdapter.isPressed();
	}
	
}
