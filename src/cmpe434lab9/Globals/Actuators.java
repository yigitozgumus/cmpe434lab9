package cmpe434lab9.Globals;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;

public class Actuators {

	private static EV3LargeRegulatedMotor leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
	private static EV3LargeRegulatedMotor rightMotor = new EV3LargeRegulatedMotor(MotorPort.D);
	private static Chassis chassis;
	
	public 	static MovePilot pilot;
	public static EV3MediumRegulatedMotor mediumRegulatedMotor = new EV3MediumRegulatedMotor(MotorPort.C); 

	public static void init(){
		float leftDiameter = 5.650f;
		float rightDiameter = 5.684f;
    	float trackWidth = 11.89f;
    	boolean reverse = true;
    	chassis = new WheeledChassis(
    		new Wheel[]{
    				WheeledChassis.modelWheel(leftMotor,leftDiameter).offset(-trackWidth/2).invert(reverse),
    				WheeledChassis.modelWheel(rightMotor,rightDiameter).offset(trackWidth/2).invert(reverse)}, 
    		WheeledChassis.TYPE_DIFFERENTIAL);

    	pilot = new MovePilot(chassis);
    	setLinearSpeed(10);
    	setAngularSpeed(40);
    	pilot.stop();
	}

	public static double getLinearSpeed() {
		return pilot.getLinearSpeed();
	}

	public static void setLinearSpeed(double linearSpeed) {
    	pilot.setLinearSpeed(linearSpeed);
	}

	public static double getAngularSpeed() {
		return pilot.getAngularSpeed();
	}

	public static void setAngularSpeed(double angularSpeed) {
    	pilot.setAngularSpeed(angularSpeed);
	}

}