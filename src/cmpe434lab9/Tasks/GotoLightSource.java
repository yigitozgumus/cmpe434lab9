package cmpe434lab9.Tasks;

import cmpe434lab9.Behaviors.MoveToLight;
import cmpe434lab9.Behaviors.SearchLight;
import cmpe434lab9.Behaviors.AvoidObstacle;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class GotoLightSource {
	
	public void run(){
		Behavior searchLight = new SearchLight(),
				 moveToLight = new MoveToLight(),
				 tavaf = new AvoidObstacle();

		Behavior[] behaviors = {searchLight,moveToLight,tavaf};
		(new Arbitrator(behaviors)).go();
	}
	
}
